EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 17 22
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 5000 2850 850  500 
U 618A7713
F0 "PSU1 Coil" 50
F1 "driver.sch" 50
F2 "VISO" I L 5000 3150 50 
F3 "GNDISO" I L 5000 3250 50 
F4 "ENABLE" I L 5000 2950 50 
F5 "MOSFET" O R 5850 2950 50 
$EndSheet
$Sheet
S 5000 3600 850  500 
U 618A7719
F0 "PSU2 Coil" 50
F1 "driver.sch" 50
F2 "VISO" I L 5000 3900 50 
F3 "GNDISO" I L 5000 4000 50 
F4 "ENABLE" I L 5000 3700 50 
F5 "MOSFET" O R 5850 3700 50 
$EndSheet
Wire Wire Line
	5000 3150 4900 3150
Wire Wire Line
	4900 3150 4900 1150
Wire Wire Line
	4900 1150 4650 1150
Wire Wire Line
	5000 3900 4900 3900
Wire Wire Line
	4900 3900 4900 3150
Connection ~ 4900 3150
Wire Wire Line
	5000 3250 4800 3250
Wire Wire Line
	4800 3250 4800 1650
Wire Wire Line
	4800 1250 4650 1250
Wire Wire Line
	5000 4000 4800 4000
Wire Wire Line
	4800 4000 4800 3250
Connection ~ 4800 3250
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 618A772B
P 4450 1250
AR Path="/618A772B" Ref="J?"  Part="1" 
AR Path="/6188919F/618A772B" Ref="J3"  Part="1" 
AR Path="/618C4248/618A772B" Ref="J5"  Part="1" 
AR Path="/618C5037/618A772B" Ref="J7"  Part="1" 
AR Path="/618C5F6A/618A772B" Ref="J9"  Part="1" 
AR Path="/618C701E/618A772B" Ref="J11"  Part="1" 
AR Path="/618C817F/618A772B" Ref="J13"  Part="1" 
AR Path="/618C92C0/618A772B" Ref="J15"  Part="1" 
F 0 "J13" H 4368 925 50  0000 C CNN
F 1 "Coil1 PWR" H 4368 1016 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 4450 1250 50  0001 C CNN
F 3 "~" H 4450 1250 50  0001 C CNN
	1    4450 1250
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 618A7731
P 5950 4550
AR Path="/618A7731" Ref="J?"  Part="1" 
AR Path="/6188919F/618A7731" Ref="J4"  Part="1" 
AR Path="/618C4248/618A7731" Ref="J6"  Part="1" 
AR Path="/618C5037/618A7731" Ref="J8"  Part="1" 
AR Path="/618C5F6A/618A7731" Ref="J10"  Part="1" 
AR Path="/618C701E/618A7731" Ref="J12"  Part="1" 
AR Path="/618C817F/618A7731" Ref="J14"  Part="1" 
AR Path="/618C92C0/618A7731" Ref="J16"  Part="1" 
F 0 "J14" V 5822 4730 50  0000 L CNN
F 1 "Coil1 Out" V 5913 4730 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-3-2.54_1x03_P2.54mm_Horizontal" H 5950 4550 50  0001 C CNN
F 3 "~" H 5950 4550 50  0001 C CNN
	1    5950 4550
	0    -1   1    0   
$EndComp
Wire Wire Line
	5850 2950 6050 2950
Wire Wire Line
	5950 4350 5950 3700
Wire Wire Line
	5950 3700 5850 3700
Wire Wire Line
	5850 4350 5850 4250
Wire Wire Line
	5850 4250 4800 4250
Wire Wire Line
	4800 4250 4800 4000
Connection ~ 4800 4000
Wire Wire Line
	4700 2950 5000 2950
Wire Wire Line
	4700 3700 5000 3700
Wire Wire Line
	6050 2950 6050 4350
Text HLabel 4700 2950 0    50   Input ~ 0
PSU1
Text HLabel 4700 3700 0    50   Input ~ 0
PSU2
$Comp
L Device:C C58
U 1 1 618D5530
P 5050 1400
AR Path="/6188919F/618D5530" Ref="C58"  Part="1" 
AR Path="/618C4248/618D5530" Ref="C59"  Part="1" 
AR Path="/618C5037/618D5530" Ref="C60"  Part="1" 
AR Path="/618C5F6A/618D5530" Ref="C61"  Part="1" 
AR Path="/618C701E/618D5530" Ref="C62"  Part="1" 
AR Path="/618C817F/618D5530" Ref="C63"  Part="1" 
AR Path="/618C92C0/618D5530" Ref="C64"  Part="1" 
F 0 "C63" H 5165 1446 50  0000 L CNN
F 1 "10u" H 5165 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.33x2.70mm_HandSolder" H 5088 1250 50  0001 C CNN
F 3 "~" H 5050 1400 50  0001 C CNN
	1    5050 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1150 5050 1150
Wire Wire Line
	5050 1150 5050 1250
Connection ~ 4900 1150
Wire Wire Line
	5050 1550 5050 1650
Wire Wire Line
	5050 1650 4800 1650
Connection ~ 4800 1650
Wire Wire Line
	4800 1650 4800 1250
$EndSCHEMATC
