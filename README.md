# Coil matrix MOSFET driver PCBs
This repository contains the Kicad design files for a isolated DCDC
converter module to drive N-channel MOSFETs and a control PCB to control
up to 7 coils using GPIOs.

## Control PCB
The control PCB is designed to work with the matrix like MOSFET box. It is
inteded to switch power supplies to coils as needed. Each PCB can control
2 power supplies which are connected to 7 coils each.

The seven coils outputs are isolated from the control logic, each one
needs an independent, floating, power supply to drive it.

The control input connectors are designed to match the Adafruit GPIO
Expander Bonnet, but they can also be connected to anything else (single
5V control pin per MOSFET).

## Power supply PCB
The power supply PCB uses a DCDC converter module from Recom to convert 5V
to isolated +15V. The output is filtered to reduce the switching noise,
this also loweres the output voltage to approx. 13.5V. The output is
floating and can be used to drive N-channel MOSFETs on the high-side of
the coils.

### Version changes
#### Revision 2
- fixed the symbol and output pins of the DC-DC converter. +Vout and -Vout
  were swapped
